import React, { Component } from 'react';
import {Route, Switch} from "react-router-dom";

import Layout from "./hoc/Layout/Layout";
import PrivateRoute from "./hoc/PrivateRoute/PrivateRoute";
import Login from "./containers/Login/Login";
import Registration from "./containers/Registration/Registration";
import PhotoGallery from "./containers/PhotoGallery/PhotoGallery";
import NewPhoto from "./containers/NewPhoto/NewPhoto";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <PrivateRoute path={'/'} exact component={PhotoGallery} />
          <PrivateRoute path={'/photos/new'} exact component={NewPhoto} />
          <PrivateRoute path={'/photos/:id'} component={PhotoGallery} />
          <Route path={'/login'} exact component={Login} />
          <Route path={'/registration'} exact component={Registration} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
import React from 'react';

const Substrate = props => {
  return (
    <div style={{position: 'fixed',
                top: 0, right: 0, bottom: 0, left: 0,
                background: 'rgba(0, 0, 0, 0.5)',
                zIndex: 10000}}>
      {props.children}
    </div>
  )
};

export default Substrate;
import React from 'react';
import {Image} from "react-bootstrap";

const FullImage = props => {
  return (
    <div style={{background: 'white', width: '70%', margin: '30px auto', padding: '30px', textAlign: 'center'}}>
      <Image
        onClick={props.clicked}
        src={props.image}
        thumbnail
      />
    </div>
  )
};

export default FullImage;
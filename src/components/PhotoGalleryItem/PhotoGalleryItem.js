import React from 'react';
import {Col, Image} from "react-bootstrap";
import {Link} from "react-router-dom";

import config from "../../config";

const PhotoGalleryItem = props => {
  return (
    <Col md={3}>
      <Image
        onClick={props.clicked}
        src={config.apiUrl + '/uploads/' + props.image}
        thumbnail
      />
      <h4>{props.title}</h4>
      <h4>By: <Link to={`/photos/${props.author._id}`} onClick={props.changeAuthor}>
          {props.author.displayName}
        </Link></h4>
      {props.isOwner &&
        <p onClick={props.remove} style={{color: 'red', cursor: 'pointer'}}>Remove</p>
      }
    </Col>
  );
};

export default PhotoGalleryItem;
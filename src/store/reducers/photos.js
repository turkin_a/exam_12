import * as actionTypes from "../actions/actionTypes";

const initialState = {
  currentPhotos: [],
  currentAuthor: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PHOTOS_SUCCESS:
      return {...state, currentPhotos: action.photos, currentAuthor: action.author};
    default:
      return state;
  }
};

export default reducer;
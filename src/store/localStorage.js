export const saveState = state => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('photo-gallery-state', serializedState);
  } catch (e) {
    console.log('Could not save state');
  }
};

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('photo-gallery-state');
    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};
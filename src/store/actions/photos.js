import {push} from "react-router-redux";

import axios from '../../axios-api';
import * as actionTypes from './actionTypes';

const fetchPhotosSuccess = (photos, author) => {
  return {type: actionTypes.FETCH_PHOTOS_SUCCESS, photos, author};
};

export const fetchPhotos = () => {
  return dispatch => {
    return axios.get('/photos').then(
      response => {
        dispatch(fetchPhotosSuccess(response.data, ''));
      },
      error => {
        console.log(error);
      }
    )
  }
};

export const fetchPhotosById = id => {
  return dispatch => {
    return axios.get(`/photos/${id}`).then(
      response => {
        dispatch(fetchPhotosSuccess(response.data));
      },
      error => {
        console.log(error);
      }
    )
  }
};

export const createPhoto = photoData => {
  return dispatch => {
    return axios.post('/photos', photoData).then(
      response => {
        dispatch(push('/'));
      }
    );
  };
};

export const removePhoto = id => {
  console.log(id);
  return dispatch => {
    return axios.delete('/photos', id).then(
      response => {
        dispatch(push('/'));
      }
    )
  }
};
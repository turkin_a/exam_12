import axios from '../../axios-api';
import {push} from 'react-router-redux';

import * as actionTypes from './actionTypes';

const loginUserSuccess = (user, token) => {
  return {type: actionTypes.LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
  return {type: actionTypes.LOGIN_USER_FAILURE, error};
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        const data = {
          username: response.data.username,
          password: userData.password
        };
        dispatch(loginUser(data));
      },
      error => {
        const errorObj = error.response ? error.response.data : {error: 'No internet'};
        dispatch(loginUserFailure(errorObj));
      }
    )
  }
};

export const loginUser = userData => {
  console.log(userData);
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user, response.data.token));
        dispatch(push('/'));
      },
      error => {
        const errorObj = error.response ? error.response.data : {error: 'No internet'};
        dispatch(loginUserFailure(errorObj));
      }
    )
  }
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    axios.delete('/users/sessions', {headers}).then(
      response => {
        dispatch({type: actionTypes.LOGOUT_USER});
        dispatch(push('/'));
      },
      error => {
        console.log(error);
      }
    );
  }
};

export const facebookLogin = data => {
  return dispatch => {
    axios.post('/users/facebookLogin', data).then(
      response => {
        dispatch(loginUserSuccess(response.data.user, response.data.token));
        dispatch(push('/'));
      },
      error => {
        dispatch(loginUserFailure(error.response.data));
      }
    )
  };
};
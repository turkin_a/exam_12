import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import NewPhotoForm from "../../components/NewPhotoForm/NewPhotoForm";
import {createPhoto} from "../../store/actions/photos";

class NewPhoto extends Component {
  createPhoto = photoData => {
    this.props.onPhotoCreated(photoData);
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New photo</PageHeader>
        <NewPhotoForm
          userId={this.props.user._id}
          onSubmit={this.createPhoto}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onPhotoCreated: photoData => dispatch(createPhoto(photoData))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPhoto);
import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Grid, PageHeader} from "react-bootstrap";
import {fetchPhotos, fetchPhotosById, removePhoto} from "../../store/actions/photos";

import PhotoGalleryItem from '../../components/PhotoGalleryItem/PhotoGalleryItem';
import Substrate from "../../components/UI/Modal/Substrate";
import FullImage from "../../components/UI/Modal/FullImage";
import config from "../../config";

class PhotoGallery extends Component {
  state = {
    image: '',
    author: ''
  };

  componentDidMount() {
    if (this.props.match.params.id) {
      this.props.onFetchPhotosById(this.props.match.params.id);
    } else {
      this.props.onFetchPhotos();
    }
  }

  componentDidUpdate() {
    if (this.props.match.params.id) {
      this.props.onFetchPhotosById(this.props.match.params.id);
    } else {
      this.props.onFetchPhotos();
    }
  }

  showImage = image => {
    this.setState({image});
  };

  hideImage = () => {
    this.setState({image: ''});
  };

  setAuthor = author => {
    this.setState({author: author.displayName});
    this.props.onFetchPhotosById(author._id);
  };

  removePhoto = id => {
    this.props.onRemovePhoto(id);
  };

  render() {
    return (
      <Fragment>
        {this.state.image &&
          <Substrate>
            <FullImage
              image={this.state.image}
              clicked={() => this.hideImage()}
            />
          </Substrate>
        }

        <PageHeader>
          Gallery {this.state.author && `by ${this.state.author}`}
        </PageHeader>

        <Grid>
          {this.props.photos.map(photo => (
            <PhotoGalleryItem
              key={photo._id}
              id={photo._id}
              title={photo.title}
              image={photo.image}
              author={photo.author}
              isOwner={photo.author._id === this.props.user._id}
              remove={() => this.removePhoto(photo._id)}
              clicked={() => this.showImage(config.apiUrl + '/uploads/' + photo.image)}
              changeAuthor={() => this.setAuthor(photo.author)}
            />
          ))}
        </Grid>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    photos: state.photos.currentPhotos
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchPhotos: () => dispatch(fetchPhotos()),
    onFetchPhotosById: (id) => dispatch(fetchPhotosById(id)),
    onRemovePhoto: (id) => dispatch(removePhoto(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoGallery);